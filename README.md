PHAT Framework is a set of tools (coded using Java) to model and simulate activities of daily living.
The main components are:

- **SociAALML Editor** is a graphical editor to model the elements for the simulation.
- **PHAT Simulator** is a simulator developed from scratch using jMonkeyEngine.
- **PHAT Generator** is a tool that transforms the model defined with SociAALML in java code. The code extends PHAT Simulator and can be simulated.

### REQUIREMENTS:

- Java 1.7
- Maven
- Ant
- PHAT-Simulator: https://bitbucket.org/pcampillo/phat-actors-def (It has to be installed using maven in a local repository).

### USAGE:

The sequence of commands are:

1. To open SociAALML Editor:

    **$ ant edit**
    
    Once the editor is opend, you can model and and save at any point.

2. To generate the java code to the simulation using the las model saved:
    
    **$ mvn clean compile**

3. To launch the simulation:

    **$ mvn exec:java -Dexec.mainClass="phat.sim.Main<SimulationName\>PHATSimulation"**

    where *<SimulationName\>* is the name of the simulation diagram to be simulated. 
By default, there are one diagram defined called "Sim1". So, the default way to run a simulation is typing:

    **$ mvn exec:java -Dexec.mainClass="phat.sim.MainSim1PHATSimulation"**
